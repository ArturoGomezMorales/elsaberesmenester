<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<f:view>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h:outputFormat value="Nombre: {0}, Apellidos: {1}, Correo: {2}, Numero: {3}">
			 <f:param value="#{data.nombre}" />
			 <f:param value="#{data.apellidos}" />
			 <f:param value="#{data.correo}" />
			 <f:param value="#{data.numero}"/>
	     </h:outputFormat>
		
	    
    <h:form>
		<h:commandButton action="index" value="detalles" />
	</h:form>
</body>
</html>
</f:view>