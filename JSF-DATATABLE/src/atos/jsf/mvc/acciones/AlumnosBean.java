package atos.jsf.mvc.acciones;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import atos.jsf.mvc.modelo.Alumno;

@ManagedBean(name = "data")
@SessionScoped
public class AlumnosBean {
	
	
	private String nombre;
	private String apellidos;
	private String correo;
	private String numero;

	private List<Alumno> students;

	public List<Alumno> getStudents() {
		students = new ArrayList<>();

		students.add(new Alumno(0,"Daniel", "Lopez Sanz", "daniel@telefonica.net", "678112233"));

		students.add(new Alumno(1,"Elena", "Sanz Garcia", "elena@telefonica.net", "678445566"));

		students.add(new Alumno(2,"Sergio", "Garcia Arranz", "sergio@telefonica.net", "678778899"));
		
		return students;
	}

	public void setStudents(List<Alumno> students) {
		this.students = students;
	}
	
	
	public String outcome(){
			
			FacesContext fc = FacesContext.getCurrentInstance();
			this.nombre = getNombreParam(fc);
			this.apellidos = getApellidosParam(fc);
			this.correo = getCorreoParam(fc);
			this.numero = getNumeroParam(fc);
			
			return "alumno";
		}
	
	
	public String getNombreParam(FacesContext fc){
		
		Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
		return params.get("nombre");
		
	}
	public String getApellidosParam(FacesContext fc){
			
			Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
			return params.get("apellidos");
			
		}
	public String getCorreoParam(FacesContext fc){
		
		Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
		return params.get("correo");
		
	}
	public String getNumeroParam(FacesContext fc){
		
		Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
		return params.get("numero");
		
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}
